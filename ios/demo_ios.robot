*** Settings ***
Library  AppiumLibrary

*** Keywords ***
Open App
    Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  platformVersion=${PLATFORM_VERSION}  deviceName=${DEVICE_NAME}  app=${APP_LOCATION}  bundleId=${BUNDLE_ID}
    Wait Until Page Contains  Welcome to Firefox

Verify welcome Page
    Element Should Be Visible  id=Welcome to Firefox
    Element Should Be Visible  id=Fast, private, and on your side.
    Element Should Be Enabled  id=signUpOnboardingButton
    Element Should Be Enabled  id=signInOnboardingButton
    Element Should Be Enabled  id=nextOnboardingButton
    ${welcome_text}=  Get Text  id=Welcome to Firefox
    Log To Console  Hello Appium and ${welcome_text}

*** Variable ***
${REMOTE_URL}           http://localhost:4723/wd/hub      # URL to appium server
${PLATFORM_NAME}        iOS
${PLATFORM_VERSION}     13.3
${DEVICE_NAME}          iPhone 8 Plus
${APP_LOCATION}         ~/Library/Developer/Xcode/DerivedData/Client-hepmrdajiwfrincqfvtdvfyhnmsc/Build/Products/Fennec-iphonesimulator/Client.app
${BUNDLE_ID}            org.mozilla.ios.Fennec

*** Test Case ***
Demo ios automation test
    Open App
    Verify welcome Page
    Close All Applications