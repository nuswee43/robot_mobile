*** Settings ***
Library  AppiumLibrary

*** Keywords ***
Open App
    Open Application  ${REMOTE_URL}  platformName=${PLATFORM_NAME}  platformVersion=${PLATFORM_VERSION}  deviceName=${DEVICE_NAME}  app=${APP_LOCATION}  appPackage=${APP_PACKAGE}  appActivity=${APP_ACTIVITY}
    Wait Until Element Is Visible  id=com.kolloware.hypezigapp:id/item_filter

Download Mock Data
    Click Element  id=com.kolloware.hypezigapp:id/nav_download
    Click Element  xpath=//android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[3]
    Wait Until Page Contains  Result: Success  500s
    Element Attribute Should Match  xpath=//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[3]
    ...    text  Result: Success
    ${status}=  Get Text  //android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView[3]
    Log To Console  Download ${status} and Hello Appium

*** Variable ***
${REMOTE_URL}        http://localhost:4723/wd/hub      # URL to appium server
${PLATFORM_NAME}     Android
${PLATFORM_VERSION}  9.0.0
${DEVICE_NAME}       Google_Pixel
${APP_LOCATION}      ~/Downloads/com.kolloware.hypezigapp_12.apk
${APP_PACKAGE}       com.kolloware.hypezigapp
${APP_ACTIVITY}      com.kolloware.hypezigapp.ui.MainActivity

*** Test Case ***
Demo android automation test
    Open App
    Download Mock Data
    Close All Applications